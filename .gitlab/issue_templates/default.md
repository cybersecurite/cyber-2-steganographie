## Problème rencontré

_Décrire ici le problème rencontré_

### Étapes pour reproduire le problème

1.
2.
3.

### Ce que j'ai obtenu

_Insérer ici une image du problème_

### Ce que j'attendais

_Inserer ici une description du résultat attendu_

### Proposition de correction

_Insérer ici une proposition de correction_

Si vous êtes à l'aise avec gitlab, vous pouvez écrire votre proposition dans une requête de fusion.

/cc @charles.poulmaire