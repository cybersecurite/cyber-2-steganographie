---
hide:
  - toc
author: à compléter
title: Musique
---

# Challenge : musique

![](../chall_musique.png){: .center }


<strong>Mercredi 25 septembre 2024, 18h30.</strong><br>
Guillaume pénétra dans le Metropolitan Opera et se dirigea vers l'accueil afin d'y retirer son billet.

Sitôt son nom donné, le responsable de l'accueil lui répondit :

<span style='font-style:italic;'>&laquo; La personne avec qui vous aviez rendez-vous n'a malheureusement pas pu venir. Elle vous propose de vous voir un autre jour et nous a remis ceci à votre attention. &raquo;</span><br>

Le responsable tendit alors à Guillaume, en plus du <a href='../Tosca.pdf' download='Tosca.pdf'>livret de l'opéra</a>, une petite enveloppe marron sur laquelle n'était inscrit aucun signe distinctif, à part les deux initiales TK... TheKalife.

Le responsable de l'accueil reprit alors :

<span style='font-style:italic;'>&laquo; La personne nous a également dit de vous dire de bien profiter du spectacle. Il y a des grands airs remarquables ! &raquo;</span><br>

Guillaume remercia et se dirigea vers la salle pour rejoindre sa place. Un fois assis, il ouvrit l'enveloppe. Celle-ci contenait une clé USB et une feuille de papier sur laquelle étaient écrites ces quelques notes de musique :

<center><img src='../chall_musique_portee.png' style='width:80%;height:auto;'></center>

<br>
Guillaume connecta la clé sur son petit ordinateur de poche et découvrit un <a href='../RDV.zip' download='RDV.zip'>fichier zippé</a> protégé par un mot de passe.

!!! note "Votre objectif"
    Découvrir la date et le prochain lieu du rendez-vous entre Guillaume et TheKalife.

    Le flag est la ville (en français) et la date (format AAAAMMJJ) où a eu lieu la première de l'oeuvre

    <span style='font-style:italic;'>Format du flag : venise_19820625</span>



<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<br>
<p>
    <form id="form_musique1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p><strong>Mardi 1 octobre 2024, 11h45.</strong><br>
Guillaume descendit les marches de son avion qui venait d'arriver à l'aéroport de Barcelone. Allait-il enfin pouvoir avoir une confrontation avec son ennemi ou... ses ennemis... ?</p>

!!! danger "A retenir"

    La stéganographie est l'art de masquer une information dans une autre.

    Les premiers éléments de stéganographie sont rapportés par l'historien grec Hérodote (484-445 av. J.-C.).

    De nos jours, les techniques ont évolué rendant l'efficacité de la stéganographie beaucoup plus performante.

</div>

<script src='../script_chall_musique.js'></script>