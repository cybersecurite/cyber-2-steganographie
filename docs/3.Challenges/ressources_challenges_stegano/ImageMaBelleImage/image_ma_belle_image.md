---
hide:
  - toc
author: à compléter
title: Image, ma belle image
---

# Challenge : image, ma belle image

![](../image_livre.png){: .center }

!!! note "Votre objectif"
    Pour chacune des situations ci-dessous, déterminez le flag caché dans l'image.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Situation 1 : observation</h2>
<br>
<center><img src = '../image_1.png' style='width:50%;height:auto;'></center>
<br>
<p><strong>Indice :</strong> avez vous besoin d'une loupe ?</p>
<br>
<p>
    <form id="form_image_ma_belle_image1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Situation 2 : des données, partout !</h2>
<br>
<center><img src = '../image_2.png' style='width:40%;height:auto;'></center>
<br>
<p><strong>Indice :</strong> une photo n’est pas qu’une image...</p>
<br>

<p>
    <form id="form_image_ma_belle_image2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>

<div id='Partie3' hidden>
<br>
<h2>Situation 3 : différence de perroquets ?</h2>
<br>
<center>
<img src = '../image_3.png' style='width:40%;height:auto;'> <img src = '../image_3_bis.png' style='width:40%;height:auto;'></center>
<br>
<p><strong>Indice 1 :</strong></p>

```python
68 74 74 70 73 3a 2f 2f 64 6f 75 62 73 61 73 74 75 63 65 73 2e
66 72 2f 69 6e 64 65 78 2e 70 68 70 2f 69 6e 66 6f 72 6d 61 74
69 71 75 65 2f 61 73 74 75 63 65 73 2d 69 6e 66 6f 72 6d 61 74
69 71 75 65 73 2f 37 35 2d 6d 61 6b 69 6e 67 2d 73 74 65 67 61
6e 6f 67 72 61 70 68 79 2d 77 69 74 68 2d 67 69 6d 70
```

<br>
<p><strong>Indice 2 :</strong> ce perroquet s’appelle Vigenère et est l’une des clés de ce mystère.</p>
<br>
<p>
    <form id="form_image_ma_belle_image3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>

<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez récupéré tous les flags !<br><br>
La stéganographie n'a pas de secret pour vous !</p>

!!! danger "A retenir"

    La stéganographie est l'art de masquer une information dans une autre.

    Les premiers éléments de stéganographie sont rapportés par l'historien grec Hérodote (484-445 av. J.-C.).

    De nos jours, les techniques ont évolué rendant l'efficacité de la stéganographie beaucoup plus performante.

</div>

<script src='../script_chall_image_ma_belle_image.js'></script>