const flags = {'image_ma_belle_image1':['Tu_@_tr0uv2',1,1],  // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
				'image_ma_belle_image2':['M1a0u',2,1],
				'image_ma_belle_image3':['trouver',3,0]};
 

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_image_ma_belle_image1.addEventListener( "submit", check_and_update);
form_image_ma_belle_image2.addEventListener( "submit", check_and_update);
form_image_ma_belle_image3.addEventListener( "submit", check_and_update);