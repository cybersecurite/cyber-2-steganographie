---
hide:
  - toc
author: à compléter
title: Gif
---

# Challenge : gif

![](../chall_gif_intro.png){: .center }


<strong>Vendredi 20 septembre 2024, 8h15.</strong><br>
Guillaume pénétra dans son bureau et alluma son ordinateur avec un étrange pressentiment qui ne l'avait pas quitté depuis son réveil. Et il sut pourquoi lorsqu'il découvrit ce mail :<br><br>

<span style='font-style:italic;'>Mon cher Guillaume,<br><br>
Vous pensiez en avoir fini avec moi. Quelle erreur !<br>
Je vous propose une petite entrevue amicale où nous pourrions échanger nos points de vue.<br>
TK</span><br><br>

TK ! Une seule personne signait comme ceci : TheKalife, le fameux hackeur qu'il avait réussi à arrêter il y a quelques mois. A priori, celui-ci avait réussi à s'échapper...

En pièce jointe, Guillaume découvrit ce <a href='../chall_gif_gif.gif' download='chall_gif_gif.gif'>fichier</a> qu'il ouvrit après l'avoir passé à son antivirus.

!!! note "Votre objectif"
    Découvrir la date et le lieu de l'entrevue entre Guillaume et TheKalife.

    Le flag est le nom de l'interprète du rôle féminin principal ?

    <span style='font-style:italic;'>Format du flag : Natalie-Dessay</span>



<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<br>
<p>
    <form id="form_gif1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p><strong>Mercredi 25 septembre 2024, 18h30.</strong><br>
Guillaume pénétra dans le Metropolitan Opera et se dirigea vers l'accueil pour récupérer son billet et, peut-être, autre chose...</p>

!!! danger "A retenir"

    Un Gif est une succession d'images (frame) que l'on affiche pendant un certain laps de temps. En jouant sur la durée des frames, on peut facilement cacher une information.

</div>

<script src='../script_chall_gif.js'></script>