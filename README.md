```mermaid
flowchart LR
    id1[[LE PROJET EST MAINTENU SUR FORGE.APPS.EDUCATION.FR]]
```

![site](site.png)

https://cybersecurite.forge.apps.education.fr/cyber/

Pour nous écrire 

forge-apps+guichet+cybersecurite-cyber-910-issue-@phm.education.gouv.fr

## Ce que le projet fait

Notre site offre des informations sur les bonnes pratiques en matière de sécurité informatique, des conseils pour protéger ses données et sa vie privée, ainsi que des astuces pour détecter et contrer les menaces en ligne.
Nous proposons également des défis interactifs pour mettre à l'épreuve vos compétences en cybersécurité, vous permettant de vous entraîner tout en vous amusant. En complément de ceux-ci, nous proposons une sélection de plateformes externes renommées, où vous pourrez aussi tester vos compétences dans des environnements variés.

Que vous soyez débutant, simple curieux ou d'un niveau avancé, notre plateforme d'apprentissage est adaptée à tous et renforcera vos connaissances et compétences en matière de cybersécurité.

Le site est organisé en cinq grandes sections :

- Bonnes pratiques
- Boîte à outils
- Challenges
- Plateformes de challenges
- Divers

```mermaid
mindmap
  root((Cybersécurité))
    )Introduction(
    )Plateformes de<br/>de challenges(
    )Challenges(
    )Boite à outils(
    )Divers(
```


## Qui maintient et contribue au projet

```mermaid
flowchart LR
    id["pascal.remy@ac-versailles.fr ❤ "]
```

```mermaid
flowchart LR
    id["charles.poulmaire@ac-versailles.fr ❤ "]
```

```mermaid
flowchart LR
    id["marius.monnier@ac-versailles.fr ❤ "]
```


## Contributions

Ne pas s’inquiéter ! Il y a toutes sortes de façons de s’impliquer dans ce projet open source.

La communauté autour du site Cybersécurité est ouverte et prête à accompagner toutes les bonnes volontés intéressées pour améliorer l'outil.

Les méthodes de contribution sont disponibles sur https://forge.apps.education.fr/cybersecurite/cyber/-/blob/main/CONTRIBUTING.md

Vous pouvez nous contacter à forge-apps+guichet+cybersecurite-cyber-910-issue-@phm.education.gouv.fr


